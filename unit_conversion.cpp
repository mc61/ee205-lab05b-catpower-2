///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file unit_conversion.cpp
/// @version 1.0
///
/// @see https://en.wikipedia.org/wiki/Units_of_energy
/// @see https://en.wikipedia.org/wiki/List_of_unusual_units_of_measurement#Energy
///
/// @author Caleb Mueller <mc61@hawaii.edu>
/// @date   14_FEB_2022
///////////////////////////////////////////////////////////////////////////////

#include "unit_conversion.h"

void print_instructions()
{
    printf("Energy converter\n");
    printf("Usage:  catPower fromValue fromUnit toUnit\n");
    printf("   fromValue: A number that we want to convert\n");
    printf("   fromUnit:  The energy unit fromValue is in\n");
    printf("   toUnit:  The energy unit to convert to\n");
    printf("\n");
    printf("This program converts energy from one energy unit to another.\n");
    printf("The units it can convert are: \n");
    printf("   j = Joule\n");
    printf("   e = eV = electronVolt\n");
    printf("   m = MT = megaton of TNT\n");
    printf("   g = GGE = gasoline gallon equivalent\n");
    printf("   f = foe = the amount of energy produced by a supernova\n");
    printf("   c = catPower = like horsePower, but for cats\n");
    printf("\n");
    printf("To convert from one energy unit to another, enter a number \n");
    printf("it's unit and then a unit to convert it to.  For example, to\n");
    printf("convert 100 Joules to GGE, you'd enter:  $ catPower 100 j g\n");
    printf("\n");
}

double convertToJoules(const double initialValue, const char fromUnit)
{
    double valueInJoules = 0;
    switch (fromUnit)
    {
    case JOULE:
    {
        // Conversion of Joules to Joules
        valueInJoules = initialValue;
        break;
    }
    case ELECTRON_VOLT:
    {
        // Conversion of Electron Volts to Joules
        valueInJoules = fromElectronVoltsToJoule(initialValue);
        break;
    }
    case MEGATON:
    {
        // Conversion of Mega Tons to Joules
        valueInJoules = fromMegatonToJoule(initialValue);
        break;
    }
    case GAS_GALLON_EQUIV:
    {
        // Conversion of Gallons of Gasoline to Joules
        valueInJoules = fromGgeToJoule(initialValue);
        break;
    }
    case FOE:
    {
        // Conversion of Foes to Joules
        valueInJoules = fromFoeToJoule(initialValue);
        break;
    }
    case CATPOWER:
    {
        // Conversion of Cat Power to Joules
        valueInJoules = fromCatPowerToJoule(initialValue);
        break;
    }
    default:
        printf("Unknown fromUnit [%c]", fromUnit);
        break;
    }
    return valueInJoules;
}

double convertFromJoules(const double valueInJoules, const char toUnit)
{
    double outputValue;
    switch (toUnit)
    {
    case JOULE:
    {
        // Conversion of Joules to Joules
        outputValue = valueInJoules;
        break;
    }
    case ELECTRON_VOLT:
    {
        // Conversion of Joules to Electron Volts
        outputValue = fromJouleToElectronVolts(valueInJoules);
        break;
    }
    case MEGATON:
    {
        // Conversion of Joules to Mega Tons
        outputValue = fromJouleToMegaton(valueInJoules);
        break;
    }
    case GAS_GALLON_EQUIV:
    {
        // Conversion of Joules to Gallons of Gasoline
        outputValue = fromJouleToGge(valueInJoules);
        break;
    }
    case FOE:
    {
        // Conversion of Joules to Foes
        outputValue = fromJouleToFoe(valueInJoules);
        break;
    }
    case CATPOWER:
    {
        // Conversion of Joules to Cat Power
        outputValue = fromJouleToCatPower(valueInJoules);
        break;
    }
    default:
        break;
    }
    return outputValue;
}

bool unitInputIsValid(const char unitToValidate)
{
    bool isValid = false;
    if (
        unitToValidate == CATPOWER ||
        unitToValidate == ELECTRON_VOLT ||
        unitToValidate == FOE ||
        unitToValidate == GAS_GALLON_EQUIV ||
        unitToValidate == JOULE ||
        unitToValidate == MEGATON)
    {
        isValid = true;
    }
    return isValid;
}

bool valueInputIsValid(const char valueToValidate)
{
    if (valueToValidate == 0.0)
    {
        return 0;
    }
    return 1;
}