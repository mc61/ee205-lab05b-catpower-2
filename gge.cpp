///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file catPower.cpp
/// @version 1.0
///
/// @author Caleb Mueller <mc61@hawaii.edu>
/// @date 14_FEB_2022
///////////////////////////////////////////////////////////////////////////////

#include "gge.h"

double fromGgeToJoule(double gge)
{
    return gge * JOULES_PER_GALLONSGAS;
}

double fromJouleToGge(double joules)
{

    return joules / JOULES_PER_GALLONSGAS;
}