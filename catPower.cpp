///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// Usage:  catPower fromValue fromUnit toUnit
///    fromValue: A number that we want to convert
///    fromUnit:  The energy unit fromValue is in
///    toUnit:  The energy unit to convert to
///
/// Result:
///   Print out the energy unit conversion
///
/// Example:
///   $ ./catPower 3.45e20 e j
///   3.45E+20 e is 55.2751 j
///
/// Compilation:
///   $ g++ -o catPower catPower.cpp
///
/// @file catPower.cpp
/// @version 1.0
///
/// @see https://en.wikipedia.org/wiki/Units_of_energy
/// @see https://en.wikipedia.org/wiki/List_of_unusual_units_of_measurement#Energy
///
/// @author Caleb Mueller <mc61@hawaii.edu>
/// @date   14_FEB_2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>

#include "unit_conversion.h"

int main(int argc, char *argv[])
{
   if (argc != 4) // program was involked with the wrong number of inputs
   {
      print_instructions();
   }
   else // program was involked with correct number of inputs
   {
      double fromValue = atof(argv[1]);
      char fromUnit = argv[2][0];
      char toUnit = argv[3][0];

      // Debug section
#ifdef DEBUG
      printf("fromValue = [%lG]\n", fromValue);
      printf("fromUnit = [%c]\n", fromUnit);
      printf("toUnit = [%c]\n", toUnit);
      printf("commonValue = [%lG] joule\n", convertToJoules(fromValue, fromUnit));

#endif

      // Input validation
      bool allInputIsValid = true;

      if (unitInputIsValid(fromUnit) == false)
      {
         allInputIsValid = false;
         printf("Unknown fromUnit [%c]\n", fromUnit);
      }

      if (unitInputIsValid(toUnit) == false)
      {
         allInputIsValid = false;
         printf("Unknown toUnit [%c]\n", toUnit);
      }

      if (fromValue == 0)
      {
         allInputIsValid = false;
         printf("Invalid fromValue\n");
      }
      // End of Input Validation

      // Input has passed snuff, conversion has green light
      if (allInputIsValid)
      {
         double toValue = convertFromJoules(convertToJoules(fromValue, fromUnit), toUnit);
         printf("%lG %c is %.4lG %c\n", fromValue, fromUnit, toValue, toUnit);
      }
   }

   return 0;
}