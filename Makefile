###############################################################################
###          University of Hawaii, College of Engineering
### @brief   Lab 05b - CatPower 2 - EE 205 - Spr 2022
###
### @author Caleb Mueller <mc61@hawaii.edu>
### @date   14_FEB_2022
###
### @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################
CC = g++
CFLAGS = -g -Wall -Wextra
TARGET = catPower

all: $(TARGET)

cat.o: cat.cpp cat.h
	$(CC) $(CFLAGS) -c cat.cpp

ev.o: ev.cpp ev.h
	$(CC) $(CFLAGS) -c ev.cpp

foe.o: foe.cpp foe.h
	$(CC) $(CFLAGS) -c foe.cpp

gge.o: gge.cpp gge.h
	$(CC) $(CFLAGS) -c gge.cpp

megaton.o: megaton.cpp megaton.h
	$(CC) $(CFLAGS) -c megaton.cpp

unit_conversion.o: unit_conversion.cpp unit_conversion.h
	$(CC) $(CFLAGS) -c unit_conversion.cpp

catPower.o: catPower.cpp cat.h ev.h foe.h gge.h joule.h megaton.h unit_conversion.h
	$(CC) $(CFLAGS) -c catPower.cpp

catPower: catPower.o cat.o ev.o foe.o gge.o megaton.o unit_conversion.o
	$(CC) $(CFLAGS) -o $(TARGET) catPower.o cat.o ev.o foe.o gge.o megaton.o unit_conversion.o

clean:
	rm -f $(TARGET) *.o

test: catPower
	@./catPower 3.14 j j | grep -q "3.14 j is 3.14 j"
	@./catPower 3.14 j e | grep -q "3.14 j is 1.96E+19 e"
	@./catPower 3.14 j m | grep -q "3.14 j is 7.512E-16 m"
	@./catPower 3.14 j g | grep -q "3.14 j is 2.589E-08 g"
	@./catPower 3.14 j f | grep -q "3.14 j is 3.14E-44 f"
	@./catPower 3.14 j c | grep -q "3.14 j is 0 c"
	@./catPower 3.14 j x | grep -q "Unknown toUnit \[x\]"
	@./catPower 3.14 m j | grep -q "3.14 m is 1.313E+16 j"
	@./catPower 3.14 g e | grep -q "3.14 g is 2.377E+27 e"
	@./catPower 3.14 f m | grep -q "3.14 f is 7.512E+28 m"
	@./catPower 3.14 c g | grep -q "3.14 c is 0 g"
	@./catPower 3.14 x f | grep -q "Unknown fromUnit \[x\]"
	echo "All tests pass"