///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file catPower.cpp
/// @version 1.0
///
/// @author Caleb Mueller <mc61@hawaii.edu>
/// @date 14_FEB_2022
///////////////////////////////////////////////////////////////////////////////

#pragma once

// Conversion Factor
const double JOULES_PER_GALLONSGAS = 1.213e8;
const char GAS_GALLON_EQUIV = 'g';

extern double fromGgeToJoule(double gge);

extern double fromJouleToGge(double joules);
