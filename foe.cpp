///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file catPower.cpp
/// @version 1.0
///
/// @author Caleb Mueller <mc61@hawaii.edu>
/// @date 14_FEB_2022
///////////////////////////////////////////////////////////////////////////////

#include "foe.h"

double fromFoeToJoule(double foe)
{
    return foe * JOULES_PER_FOE;
}

double fromJouleToFoe(double joules)
{
    return joules / JOULES_PER_FOE;
}