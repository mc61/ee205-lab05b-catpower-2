///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file catPower.cpp
/// @version 1.0
///
/// @author Caleb Mueller <mc61@hawaii.edu>
/// @date 14_FEB_2022
///////////////////////////////////////////////////////////////////////////////

#include "cat.h"

double fromCatPowerToJoule(double catPower)
{
    return 0 * catPower; // Cat's do no work
}

double fromJouleToCatPower(double joules)
{
    return 0 * joules; // Cat's do no work
}