///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file unit_conversion.h
/// @version 1.0
///
/// @see https://en.wikipedia.org/wiki/Units_of_energy
/// @see https://en.wikipedia.org/wiki/List_of_unusual_units_of_measurement#Energy
///
/// @author Caleb Mueller <mc61@hawaii.edu>
/// @date   14_FEB_2022
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <stdio.h>
#include <stdbool.h>

#include "cat.h"
#include "ev.h"
#include "foe.h"
#include "gge.h"
#include "joule.h"
#include "megaton.h"

void print_instructions();

extern double convertToJoules(double numToConvert, char convertUnitFrom);

extern double convertFromJoules(double valueInJoules, char toUnit);

extern bool unitInputIsValid(const char unitToValidate);

extern bool valueInputIsValid(const double valueToValidate);
