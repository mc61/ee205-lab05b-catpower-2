///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file catPower.cpp
/// @version 1.0
///
/// @author Caleb Mueller <mc61@hawaii.edu>
/// @date 14_FEB_2022
///////////////////////////////////////////////////////////////////////////////

#pragma once

// Conversion Factor
const double JOULES_PER_CATPOWER = 0.0;
const char CATPOWER = 'c';

extern double fromCatPowerToJoule(double catPower);

extern double fromJouleToCatPower(double joules);
